<?php

class DatabaseManager {
	const DATABASE_FILE = 'weihnachtsfeier.db';

	/**
	 * Lifert alle Teams als Array zurück.
	 * e.g. ['Mein Team' => ['Dirk', 'Walter', 'Herbert'], 'Gegner Team' => ['Werner', 'Luise', 'Emma']]
	 * @return die Teams
	 */
	public static function getTeams() {
		$teams = array();
		if (file_exists(DatabaseManager::DATABASE_FILE)) {
			if ($db = new SQLite3(DatabaseManager::DATABASE_FILE)) {
				$statement = $db->prepare('SELECT NAME FROM SQLITE_MASTER WHERE TYPE=\'table\' AND NAME=\'PLAYERS\'');
				$result = $statement->execute();
				$tableExists = $array = $result->fetchArray();
				$statement->close();
				if ($tableExists) {
					$statement = $db->prepare('SELECT P.NAME AS NAME, T.NAME AS TEAM FROM PLAYERS AS P JOIN TEAMS AS T ON P.TEAM = T.ID');
					$result = $statement->execute();
					while($array = $result->fetchArray(SQLITE3_ASSOC)) {
						if (array_key_exists('NAME', $array) && array_key_exists('TEAM', $array)) {
							if (array_key_exists($array['TEAM'], $teams)) {
								$teams[$array['TEAM']][] = $array['NAME'];
							} else {
								$teams[$array['TEAM']] = array($array['NAME']);
							}
						}
					}
					$statement->close();
				}
				$db->close();
			}
		}
		return $teams;
	}

	/**
	 * Fügt der Datenbank ein Team hinzu.
	 * @param $name der Name des Teams
	 * @param $members die Teammitglieder (e.g. ['Dirk', 'Walter', 'Herbert'])
	 * @return <strong>true</strong> wenn erfolgreich, <strong>false</strong> andernfalls
	 */
	public static function addTeam($name, $members) {
		$success = false;
		if ($db = new SQLite3(DatabaseManager::DATABASE_FILE)) {
			$success = true;
			$db->exec('CREATE TABLE IF NOT EXISTS TEAMS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME VARCHAR)');
			$db->exec('CREATE TABLE IF NOT EXISTS PLAYERS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME VARCHAR, TEAM INTEGER NOT NULL REFERENCES TEAMS(ID))');
			if($statement = $db->prepare('INSERT INTO TEAMS (NAME) VALUES (:teamName)')) {
				$statement->bindValue(':teamName', $_POST['teamName'], SQLITE3_TEXT);
				$statement->execute();
				$statement = $db->prepare('SELECT ID FROM TEAMS WHERE NAME = :teamName');
				$statement->bindValue(':teamName', $_POST['teamName'], SQLITE3_TEXT);
				if ($result = $statement->execute()) {
					if ($array = $result->fetchArray(SQLITE3_ASSOC)) {
						if (array_key_exists('ID', $array)) {
							$teamId = $array['ID'];
						}
					}
				}
				if (isset($teamId)) {
					foreach ($members as $member) {
						$statement = $db->prepare('INSERT INTO PLAYERS (NAME, TEAM) VALUES (:name, :teamId)');
						$statement->bindValue(':name', $member, SQLITE3_TEXT);
						$statement->bindValue(':teamId', $teamId, SQLITE3_INTEGER);
						$success = $success !== false && $statement->execute();
						$statement->close();
					}
				}
			}
			$db->close();
		}
		return $success === true;
	}

	/**
	 * Entfernt das Team aus der Datenbank.
	 * Löscht alle generierten Paarungen!
	 * @param $team das Team
	 * @return <strong>true</strong> wenn erfolgreich, <strong>false</strong> andernfalls
	 */
	public static function deleteTeam($team) {
		$success = false;
		if ($db = new SQLite3(DatabaseManager::DATABASE_FILE)) {
			$db->exec('DROP TABLE IF EXISTS PAIRINGS');
			$statement = $db->prepare('DELETE FROM TEAMS WHERE NAME = :team');
			$statement->bindValue(':team', $team);
			$success = $statement->execute();
			$statement->close();
			$db->close();
		}
		return $success === true;
	}

	/**
	 * Setzt (und überschreibt) alle Spiele.
	 * @param $games die Spiele (e.g. ['Laufen' => 'Der Schnellste gewinnt', 'Werfen' => 'Der Höchste gewinnt'])
	 * @return <strong>true</strong> wenn erfolgreich, <strong>false</strong> andernfalls
	 */
	public static function setGames($games) {
		$success = false;
		if ($db = new SQLite3(DatabaseManager::DATABASE_FILE)) {
			$db->exec('DROP TABLE IF EXISTS GAMES');
			$db->exec('CREATE TABLE GAMES (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME VARCHAR, DESCRIPTION VARCHAR)');
			$success = true;
			foreach ($games as $name => $description) {
				$statement = $db->prepare('INSERT INTO GAMES (NAME, DESCRIPTION) VALUES (:game, :description)');
				$statement->bindValue(':game', $name, SQLITE3_TEXT);
				$statement->bindValue(':description', $description, SQLITE3_TEXT);
				$result = $statement->execute();
				$statement->close();
				if (!$result) {
					$success = false;
				}
			}
			$db->close();
		}
		return $success === true;
	}

	/**
	 * Liefert alle Spiele als Array zurück.
	 * e.g. <pre>['Laufen' => 'Der Schnellste gewinnt',
	 * 'Werfen' => 'Der Höchste gewinnt']</pre>
	 * @return die Spiele
	 */
	public static function getGames() {
		$games = array();
		if (file_exists(DatabaseManager::DATABASE_FILE)) {
			if ($db = new SQLite3(DatabaseManager::DATABASE_FILE)) {
				$statement = $db->prepare(
						'SELECT NAME FROM SQLITE_MASTER'
						. ' WHERE TYPE=\'table\' AND NAME=\'GAMES\'');
				$result = $statement->execute();
				$tableExists = $array = $result->fetchArray();
				$statement->close();
				if ($tableExists) {
					$statement = $db->prepare('SELECT NAME, DESCRIPTION FROM GAMES');
					$result = $statement->execute();
					while($array = $result->fetchArray(SQLITE3_ASSOC)) {
						if (array_key_exists('NAME', $array)
							&& array_key_exists('DESCRIPTION', $array)) {
							$games[$array['NAME']] = $array['DESCRIPTION'];
						}
					}
					$statement->close();
				}
				$db->close();
			}
		}
		return $games;
	}

	/**
	 * Entfernt das Spiel aus der Datenbank.
	 * Löscht alle generierten Paarungen!
	 * @param $team das Team
	 * @return <strong>true</strong> wenn erfolgreich, <strong>false</strong> andernfalls
	 */
	public static function deleteGame($game) {
		$success = false;
		if ($db = new SQLite3(DatabaseManager::DATABASE_FILE)) {
			$db->exec('DROP TABLE IF EXISTS PAIRINGS');
			$statement = $db->prepare('DELETE FROM GAMES WHERE NAME = :game');
			$statement->bindValue(':game', $game);
			$success = $statement->execute();
			$statement->close();
			$db->close();
		}
		return $success === true;
	}

	/**
	 * Generiert die Paarungen für alle Teams und Spiele.
	 * Die Struktur der zurückgegebenen Matrix sieht wie folgt aus:
	 * matrix[durchlauf][spiel] sind alle Teams, die in dem Durchlauf 'durchlauf'
	 * das Spiel 'spiel' spielen.
	 * @param $teams die Teams
	 * @param $games die Spiele
	 * @return die resultierende Matrix
	 */
	public static function generatePairings($teams, $games) {
		$matrix = [];
		$teamObjects = [];
		foreach ($teams as $name => $members) {
			$teamObjects[$name] = ['name' => $name, 'gamesPlayed' => []];
		}
		$gameObjects = [];
		foreach ($games as $name => $description) {
			$gameObjects[$name] = ['name' => $name, 'participants' => []];
		}
		$randomTeams = $teamObjects;
		shuffle($randomTeams);
		$remainingTeams = count($randomTeams);
		$matrix[0] = [];
		foreach ($gameObjects as $gameName => &$game) {
			$matrix[0][$gameName] = [];
			for ($j = min($remainingTeams, 2); $j > 0; $j--) {
				$team = $randomTeams[--$remainingTeams];
				$matrix[0][$gameName][] = $team;
			}
			DatabaseManager::addParticipants(
				$game, $matrix[0][$gameName], $teamObjects);
		}
		$run = 0;
		while (!DatabaseManager::isComplete($gameObjects, $teamObjects) && $run < 20) {
			$remainingTeams = $teamObjects;
			$matrix[++$run] = [];
			usort($gameObjects, function($a, $b) {
				return count(DatabaseManager::getUniqueParticipants($a))
						- count(DatabaseManager::getUniqueParticipants($b));
			});
			for ($i = 0; $i < count($gameObjects); $i++) {
				$game = &$gameObjects[$i];
				$matrix[$run][$game['name']] = [];
				$gamePlayedBy = array_map(function($team) use($game) {
					return [$team['name'], DatabaseManager::getPlayedBy($game, $team)];
				}, $remainingTeams);
				usort($gamePlayedBy, function($a, $b) {
					return $a[1] - $b[1];
				});
				for ($j = min(count($gamePlayedBy), 2); $j > 0; $j--) {
					$removed = $teamObjects[array_shift($gamePlayedBy)[0]];
					$remainingTeams = array_filter(
						$remainingTeams, function($team) use($removed) {
							return $team['name'] !== $removed['name'];
					});
					$matrix[$run][$game['name']][] = $removed;
				}
				DatabaseManager::addParticipants(
					$game, $matrix[$run][$game['name']], $teamObjects);
			}
		}
		return $matrix;
	}

	/**
	 * Generiert die Paarungen für alle Teams und Spiele.
	 * Die Struktur der zurückgegebenen Matrix sieht wie folgt aus:
	 * matrix[durchlauf][spiel] ist das Team, das in dem Durchlauf 'durchlauf'
	 * das Spiel 'spiel' spielt.
	 * @param $teams die Teams
	 * @param $games die Spiele
	 * @return die resultierende Matrix
	 */
	public static function generatePairingsSimplified($teams, $games) {
		if (count($teams) == count($games)) {
			return DatabaseManager::equalPairings($teams, $games);
		}
		$matrix = [];
		$teamObjects = [];
		foreach ($teams as $name => $members) {
			$teamObjects[$name] = ['name' => $name, 'gamesPlayed' => []];
		}
		$gameObjects = [];
		foreach ($games as $name => $description) {
			$gameObjects[$name] = ['name' => $name, 'participants' => []];
		}
		$randomTeams = $teamObjects;
		shuffle($randomTeams);
		$remainingTeams = count($randomTeams);
		$matrix[0] = [];
		foreach ($gameObjects as $gameName => &$game) {
			$matrix[0][$gameName] = [];
			for ($j = min($remainingTeams, 1); $j > 0; $j--) {
				$team = $randomTeams[--$remainingTeams];
				$matrix[0][$gameName][] = $team;
			}
			DatabaseManager::addParticipants(
				$game, $matrix[0][$gameName], $teamObjects);
		}
		$run = 0;
		while (!DatabaseManager::isComplete($gameObjects, $teamObjects) && $run < 15) {
			$remainingTeams = $teamObjects;
			$matrix[++$run] = [];
			usort($gameObjects, function($a, $b) {
				return count(DatabaseManager::getUniqueParticipants($a))
						- count(DatabaseManager::getUniqueParticipants($b));
			});
			for ($i = 0; $i < count($gameObjects); $i++) {
				$game = &$gameObjects[$i];
				$matrix[$run][$game['name']] = [];
				$gamePlayedBy = array_map(function($team) use($game) {
					return [$team['name'], DatabaseManager::getPlayedBy($game, $team)];
				}, $remainingTeams);
				usort($gamePlayedBy, function($a, $b) {
					return $a[1] - $b[1];
				});
				if ($i + 1 < count($gameObjects)) {
					$nextGame = $gameObjects[$i + 1];
					$tmp = $remainingTeams;
					array_shift($tmp);
					$nextGamePlayedBy = array_map(function($team) use($nextGame) {
						return [$team['name'], DatabaseManager::getPlayedBy($nextGame, $team)];
					}, $tmp);
					usort($nextGamePlayedBy, function($a, $b) {
						return $a[1] - $b[1];
					});
					usort($gamePlayedBy, function($a, $b) use($nextGamePlayedBy) {
						$alen = array_search($a, $nextGamePlayedBy);
						$blen = array_search($b, $nextGamePlayedBy);
						return $a[1] - $b[1] == 0 ? ($alen == -1 ? 1 : ($blen == -1 ? -1 : $alen - $blen)) : $a[1] - $b[1];
					});
				}
				$toRemove = min(count($gamePlayedBy), 1);
				for ($j = $toRemove; $j > 0; $j--) {
					$removed = $teamObjects[array_shift($gamePlayedBy)[0]];
					$remainingTeams = array_filter(
						$remainingTeams, function($team) use($removed) {
							return $team['name'] !== $removed['name'];
					});
					$matrix[$run][$game['name']][] = $removed;
				}
				if ($toRemove > 0) {
					DatabaseManager::addParticipants(
						$game, $matrix[$run][$game['name']], $teamObjects);
				}
			}
		}
		return $matrix;
	}

	/**
	 * Hilfsfunktion für generatePairings.
	 * Liefert, ob jedes Team schon an jedem
	 * Spiel mindestens einmal teilgenommen hat.
	 * @param $games die Spiele
	 * @param $teams die Teams
	 * @return Komplettierungsstatus der Generierung
	 */
	private static function isComplete(&$games, &$teams) {
		foreach ($teams as $team) {
			foreach ($games as $game) {;
				if (!in_array($game['name'], $team['gamesPlayed'])) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Hilfsfunktion für generatePairings.
	 * Registriert die angegebenen Teams der Teilnahme an einem Spiel.
	 * @param $game das Spiel
	 * @param $participants die teilnehmenden Teams
	 * @param $teamObjects alle Teams
	 */
	private static function addParticipants(
			&$game, &$participants, &$teamObjects) {
		for ($i = count($participants) - 1; $i >= 0; $i--) {
			$participants[$i]['gamesPlayed'][] = $game['name'];
			$teamObjects[$participants[$i]['name']]['gamesPlayed'][] = $game['name'];
		}
		$game['participants'][] = $participants;
	}

	/**
	 * Hilfsfunktion für generatePairings.
	 * Liefert, wie oft ein Team an einem Spiel teilgenommen hat.
	 * @param $game das Spiel
	 * @param $team das Team
	 * @return Anzahl der Teilnahmen des Teams an dem Spiel
	 */
	private static function getPlayedBy($game, $team) {
		var_dump($game['participants']);
		$arr = array_filter(
			$game['participants'],
			function($member) use($team) {
				for ($i = count($member) - 1; $i >= 0; $i--) {
					if ($member[$i]['name'] === $team['name']) {
						return true;
					}
				}
				return false;
			});
		return count($arr);
	}

	/**
	 * Hilfsfunktion für generatePairings.
	 * Liefert alle Teams, die mindestens einmal an dem Spiel teilgenommen haben.
	 * @param $game das Spiel
	 * @return alle Teams, die an dem Spiel teilgenommen haben
	 */
	private static function getUniqueParticipants($game) {
		$flatArray = array();
		for ($i = count($game['participants']) - 1; $i >= 0; $i--) {
			$participants = $game['participants'][$i];
			$mappedArray = array_map(function($x) {
				return $x['name'];
			}, $participants);
			$flatArray = array_merge($flatArray, $mappedArray);
		}
		return array_unique($flatArray);
	}

	private static function equalPairings($teams, $games) {
		$teamKeys = array_keys($teams);
		$gameKeys = array_keys($games);
		$matrix = [];
		$offset = 0;
		$len = count($teams);
		for ($i = 0; $i < $len; $i++) {
			$matrix[$i] = [];
			for ($j = 0; $j < $len; $j++) {
				$teamObject = [];
				$teamObject['name'] = $teamKeys[($j + $offset) % ($len)];
				$teamObject['participants'] = $teams[$teamObject['name']];
				$matrix[$i][$gameKeys[$j]] = [$teamObject];
			}
			$offset++;
		}
		return $matrix;
	}

	/**
	 * Setzt (und überschreibt) alle Paarungen.
	 * @param $matrix die Matrix der Paarungen (generiert durch generatePairings())
	 * @return <strong>true</strong> wenn erfolgreich, <strong>false</strong> andernfalls
	 */
	public static function setPairings($matrix) {
		$success = false;
		if ($db = new SQLite3(DatabaseManager::DATABASE_FILE)) {
			$success = true;
			$db->exec('DROP TABLE IF EXISTS PAIRINGS');
			$db->exec(
					'CREATE TABLE PAIRINGS ('
					. 'ID INTEGER PRIMARY KEY AUTOINCREMENT, '
					. 'RUN INTEGER, '
					. 'GAME INTEGER NOT NULL REFERENCES GAME(ID), '
					. 'TEAM INTEGER NOT NULL REFERENCES TEAMS(ID), '
					. 'FAILS INTEGER)');
			$runs = count($matrix);
			for ($i = 0; $i < $runs; $i++) {
				$run = $i;
				$games = $matrix[$i];
				foreach ($games as $game => $teams) {
					$statement = $db->prepare('SELECT ID FROM GAMES WHERE NAME = :game');
					$statement->bindValue(':game', $game, SQLITE3_TEXT);
					$result = $statement->execute();
					if (!$result) {
						return false;
					}
					$gameIdArray = $result->fetchArray(SQLITE3_ASSOC);
					$statement->close();
					if ($gameIdArray === null
							|| !is_array($gameIdArray)
							|| count($gameIdArray) !== 1) {
						return false;
					}
					$gameId = $gameIdArray['ID'];
					foreach ($teams as $team) {
						$statement = $db->prepare(
								'SELECT ID FROM TEAMS WHERE NAME = :team');
						$statement->bindValue(':team', $team['name'], SQLITE3_TEXT);
						$result = $statement->execute();
						if (!$result) {
							return false;
						}
						$teamIdArray = $result->fetchArray(SQLITE3_ASSOC);
						$statement->close();
						if ($teamIdArray === null
								|| !is_array($teamIdArray)
								|| count($teamIdArray) !== 1) {
							return false;
						}
						$teamId = $teamIdArray['ID'];
						$statement = $db->prepare(
								'INSERT INTO PAIRINGS (RUN, GAME, TEAM) '
								. 'VALUES (:run, :game, :team)');
						$statement->bindValue(':run', $run, SQLITE3_INTEGER);
						$statement->bindValue(':game', $gameId, SQLITE3_INTEGER);
						$statement->bindValue(':team', $teamId, SQLITE3_INTEGER);
						$success = $success !== false && $statement->execute();
						$statement->close();
					}
				}
			}
			$db->close();
		}
		return $success === true;
	}

	public static function getPairings($teams, $games) {
		$success = false;
		$matrix = array();
		if ($db = new SQLite3(DatabaseManager::DATABASE_FILE)) {
			$success = true;
			$statement = $db->prepare('SELECT NAME FROM SQLITE_MASTER WHERE TYPE=\'table\' AND NAME=\'PAIRINGS\'');
			$result = $statement->execute();
			$tableExists = $array = $result->fetchArray();
			$statement->close();
			if (!$tableExists) {
				return [];
			}
			$statement = $db->prepare(
				'SELECT P.RUN, G.NAME AS GAME, T.NAME AS TEAM, P.FAILS '
				. 'FROM PAIRINGS AS P '
				. 'JOIN GAMES AS G ON P.GAME = G.ID '
				. 'JOIN TEAMS AS T ON P.TEAM = T.ID');
			$result = $statement->execute();
			if ($result) {
				while ($pairing = $result->fetchArray(SQLITE3_ASSOC)) {
					$run = $pairing['RUN'];
					$game = $pairing['GAME'];
					$team = $pairing['TEAM'];
					$fails = array_key_exists('FAILS', $pairing)
							&& is_int($pairing['FAILS'])
									? ('' . $pairing['FAILS'])
									: '';
					if (!array_key_exists($run, $matrix)) {
						$matrix[$run] = [];
					}
					foreach ($games as $gameName => $gameObject) {
						if (!array_key_exists($gameName, $matrix[$run])) {
							$matrix[$run][$gameName] = [];
						}
					}
					$teamObject = [];
					$teamObject['name'] = $team;
					$teamObject['members'] = $teams[$team];
					$teamObject['fails'] = $fails;
					$matrix[$run][$game][] = $teamObject;
				}
			}
			$statement->close();
			$db->close();
		}
		return $matrix;
	}

	public static function setResult($run, $game, $fails) {
		$success = false;
		if ($db = new SQLite3(DatabaseManager::DATABASE_FILE)) {
			$statement = $db->prepare('SELECT NAME FROM SQLITE_MASTER WHERE TYPE=\'table\' AND NAME=\'GAMES\'');
			$result = $statement->execute();
			$tableExists = $array = $result->fetchArray();
			$statement->close();
			if (!$tableExists) {
				return false;
			}
			$statement = $db->prepare('SELECT NAME FROM SQLITE_MASTER WHERE TYPE=\'table\' AND NAME=\'PAIRINGS\'');
			$result = $statement->execute();
			$tableExists = $array = $result->fetchArray();
			$statement->close();
			if (!$tableExists) {
				return false;
			}
			$statement = $db->prepare('SELECT ID FROM GAMES WHERE NAME = :game');
			$statement->bindValue(':game', $game, SQLITE3_TEXT);
			$result = $statement->execute();
			if (!$result) {
				return false;
			}
			$gameIdArray = $result->fetchArray(SQLITE3_ASSOC);
			$statement->close();
			if ($gameIdArray === null
					|| !is_array($gameIdArray)
					|| count($gameIdArray) !== 1) {
				return false;
			}
			$gameId = $gameIdArray['ID'];
			$statement = $db->prepare('UPDATE PAIRINGS SET FAILS = :fails WHERE RUN = :run AND GAME = :game');
			$statement->bindValue(':fails', $fails, SQLITE3_INTEGER);
			$statement->bindValue(':run', $run, SQLITE3_INTEGER);
			$statement->bindValue(':game', $gameId, SQLITE3_TEXT);
			$success = $statement->execute();
			$statement->close();
			$db->close();
		}
		return $success === true;
	}
}

?>
