
function addPlayer() {
	var playerNames = document.getElementById("playerNames");
	var node = document.createElement("LI");
	node.id = "playerName-" + playerNames.childElementCount;
	node.class = "playerName";
	node.innerHTML = "Spieler Name<br/>"
			+ "<input class=\"playerNameText\" type=\"text\"/>";
	playerNames.appendChild(node);
}

function removePlayer() {
	var playerNames = document.getElementById("playerNames");
	var playerChildren = playerNames.children;
	var playerChildrenCount = playerChildren.length;
	if (playerChildrenCount > 1) {
		playerNames.removeChild(playerChildren[playerChildrenCount - 1]);
	}
}

function postTeam() {
	var objForm = document.createElement('FORM');
	objForm.method = 'post';
	var objInput = document.createElement('INPUT');
	objInput.type = 'hidden';
	objInput.name = 'teamName';
	objInput.value = document.getElementById('teamNameText').value;
	objForm.appendChild(objInput);
	var playerNames = document.getElementsByClassName('playerNameText');
	for (var i = 0; i < playerNames.length; i++) {
		var objInput = document.createElement('INPUT');
		objInput.type = 'hidden';
		objInput.name = "player-" + i;
		objInput.value = playerNames[i].value;
		objForm.appendChild(objInput);
	}
	document.body.appendChild(objForm);
	objForm.submit();
	document.body.removeChild(objForm);
}

function deleteTeam(team) {
	alert("delete " + team);
	var objForm = document.createElement('FORM');
	objForm.method = 'post';
	objInput = document.createElement('INPUT');
	objInput.type = 'hidden';
	objInput.name = "team-delete";
	objInput.value = team;
	objForm.appendChild(objInput);
	document.body.appendChild(objForm);
	objForm.submit();
	document.body.removeChild(objForm);
}

function addGame() {
	var gameSettings = document.getElementById("gameSettings");
	var node = document.createElement("LI");
	node.id = "gameSetting-" + gameSettings.childElementCount;
	node.class = "gameSetting";
	node.innerHTML = "<div class=\"gameName\">Spiel Name<br/>"
			+ "<input class=\"gameNameText\" type=\"text\"/></div>"
			+ "<div class=\"gameDescription\">Spiel Beschreibung<br/>"
			+ "<input class=\"gameDescriptionText\" type=\"text\"/></div></div>";
	gameSettings.appendChild(node);
}

function removeGame() {
	var gameSettings = document.getElementById("gameSettings");
	var gameChildren = gameSettings.children;
	var gameChildrenCount = gameChildren.length;
	if (gameChildrenCount > 1) {
		gameSettings.removeChild(gameChildren[gameChildrenCount - 1]);
	}
}

function postGames() {
	var objForm = document.createElement('FORM');
	objForm.method = 'post';
	var gameSettings = document.getElementById('gameSettings').children;
	for (var i = 0; i < gameSettings.length; i++) {
		var game = gameSettings[i];
		var objInput = document.createElement('INPUT');
		objInput.type = 'hidden';
		objInput.name = "game-name-" + i;
		objInput.value = getFirstClassValue(game, 'gameNameText');
		objForm.appendChild(objInput);
		objInput = document.createElement('INPUT');
		objInput.type = 'hidden';
		objInput.name = "game-description-" + i;
		objInput.value = getFirstClassValue(game, 'gameDescriptionText');
		objForm.appendChild(objInput);
	}
	document.body.appendChild(objForm);
	objForm.submit();
	document.body.removeChild(objForm);
}

function getFirstClassValue(obj, className) {
	var elements = obj.getElementsByClassName(className);
	if (elements.length === 0) {
		return null;
	}
	return elements[0].value;
}

function deleteGame(game) {
	var objForm = document.createElement('FORM');
	objForm.method = 'post';
	objInput = document.createElement('INPUT');
	objInput.type = 'hidden';
	objInput.name = "game-delete";
	objInput.value = game;
	objForm.appendChild(objInput);
	document.body.appendChild(objForm);
	objForm.submit();
	document.body.removeChild(objForm);
}

function requestPairingGeneration() {
	var objForm = document.createElement('FORM');
	objForm.method = 'post';
		objInput = document.createElement('INPUT');
		objInput.type = 'hidden';
		objInput.name = 'generate-pairings';
		objInput.value = 'please';
		objForm.appendChild(objInput);
	document.body.appendChild(objForm);
	objForm.submit();
	document.body.removeChild(objForm);
}

function requestPairingGenerationSimplified() {
	var objForm = document.createElement('FORM');
	objForm.method = 'post';
		objInput = document.createElement('INPUT');
		objInput.type = 'hidden';
		objInput.name = 'generate-pairings-simplified';
		objInput.value = 'please';
		objForm.appendChild(objInput);
	document.body.appendChild(objForm);
	objForm.submit();
	document.body.removeChild(objForm);
}
