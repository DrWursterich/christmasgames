<html>
	<head>
		<title>Weihnachtsfeierspiele Backend</title>
		<?php
			include 'DatabaseManager.php';
		?>
		<script src="backend.js"></script>
	</head>
	<body>
		<?php
			if (isset($_POST)) {
				if (array_key_exists('teamName', $_POST)
						&& array_key_exists('player-0', $_POST)) {
					$team = array();
					$count = 0;
					while (array_key_exists('player-' . $count, $_POST)) {
						$team[] = $_POST['player-' . $count++];
					}
					echo '<span><h2>',
							DatabaseManager::addTeam($_POST['teamName'], $team)
								? 'Das Team wurde erfolgreich eingetragen!'
								: 'Es gab einen unerwarteten Fehler beim Eintragen des Teams',
							'</span></h2>';
				}
				if (array_key_exists('game-name-0', $_POST)
						&& array_key_exists('game-description-0', $_POST)) {
					$games = array();
					$count = 0;
					while (array_key_exists('game-name-' . $count, $_POST)
							&& array_key_exists('game-description-' . $count, $_POST)) {
						$games[$_POST['game-name-' . $count]] = $_POST['game-description-' . $count];
						$count++;
					}
					echo '<span><h2>',
							DatabaseManager::setGames($games)
								? 'Die Spiele wurden erfolgreich eingetragen!'
								: 'Es gab einen unerwarteten Fehler beim Eintragen der Spiele',
							'</span></h2>';
				}
				if (array_key_exists('generate-pairings', $_POST)) {
					DatabaseManager::generatePairings($teams, $games);
				}
				if (array_key_exists('game-delete', $_POST)) {
					DatabaseManager::deleteGame($_POST['game-delete']);
				}
				if (array_key_exists('team-delete', $_POST)) {
					DatabaseManager::deleteTeam($_POST['team-delete']);
				}
				if (array_key_exists('generate-pairings-simplified', $_POST)) {
					if (!isset($games)) {
						$games = DatabaseManager::getGames();
					}
					if (!isset($teams)) {
						$teams = DatabaseManager::getTeams();
					}
					$matrix = DatabaseManager::generatePairingsSimplified($teams, $games);
					if (!DatabaseManager::setPairings($matrix)) {
						echo "<h1>PAARUNGS-GENERIERUNG IST FEHLGESCHLAGEN!</h1>";
					} else {
						echo "<span><strong>Paarungen wurden erfolgreich generiert</strong></span>";
					}
				}
				$failsLength = strlen('fails-update-');
				foreach ($_POST as $key => $value) {
					if (substr($key, 0, $failsLength) === 'fails-update-') {
						$key = substr($key, $failsLength);
						$run = substr($key, 0, strpos($key, '-'));
						$game = substr($key, strlen($run) + 1);
						DatabaseManager::setResult($run, $game, $value);
					}
				}
				unset($_POST);
			}

			if (!isset($games)) {
				$games = DatabaseManager::getGames();
			}
			if (!isset($teams)) {
				$teams = DatabaseManager::getTeams();
			}
			$matrix = DatabaseManager::getPairings($teams, $games);
		?>

		<h2>Spiele</h2>
		<h3>Spiele Definieren</h3>
		<div id="gameDetails">
			<div class="gameControlls">
				<button onclick="addGame()" title="Spiel hinzufügen"> + </button>
				<button onclick="removeGame()" title="Spiel entfernen"> - </button>
				<button onclick="postGames()" title="Änderungen übernehmen">Übernehmen</button>
			</div>
			<ul id="gameSettings">
			</ul>
		</div>
		<?php
			echo '<h3>Eingetragene Spiele</h3>';
			if (sizeof($games) > 0) {
				echo '<ul>';
				foreach ($games as $name => $description) {
					echo '<li><div><strong>',
							htmlspecialchars($name),
							'</strong>&nbsp&nbsp',
							'<span class="singleGameControlls"><button onclick="deleteGame(\'',
							$name,
							'\')" title="löscht auch alle Paarungen">Spiel Löschen</button>',
							'</span><br><p>',
							htmlspecialchars($description),
							'</p></div></li>';
				}
				echo '</ul>';
			} else {
				echo '<span>Es sind noch keine Spiele eingetragen</span>';
			}
		?>

		<h2>Teams</h2>
		<h3>Team hinzufügen</h3>
		<div id="teamDetails">
			<div class="teamName">
				Team Name<br/>
				<input type="text" id="teamNameText"/><br/><br/>
			</div>
			<div id="teamControlls">
				<button onclick="addPlayer()" title="Spieler hinzufügen"> + </button>
				<button onclick="removePlayer()" title="Spieler entfernen"> - </button>
				<button onclick="postTeam()" title="Team hinzufügen">Abschicken</button>
			</div>
			<ul id="playerNames">
			</ul>
		</div>
		<?php
			if (sizeof($teams) > 0) {
				echo '<h3>Eingetragene Teams</h3><ul>';
				foreach ($teams as $name => $members) {
					echo '<li><strong>', htmlspecialchars($name), '</strong>&nbsp&nbsp',
						'<span class="singleTeamControlls"><button onclick="deleteTeam(\'',
						$name,
						'\')" title="löscht auch alle Paarungen">Team Löschen</button></span>',
						'<ul>';
						foreach ($members as $member) {
							echo '<li>', htmlspecialchars($member), '</li>';
						}
					echo '</ul></li>';
				}
				echo '</ul>';
			}
		?>

		<div id="pairingsContainer">
			<?php if($matrix && is_array($matrix) && count($matrix) > 0) {
				echo '<table><caption>Paarungen</caption><tr><th>Spiel</th>';
				$runs = count($matrix);
				for ($i = 0; $i < $runs; $i++) {
					echo '<th>Durchlauf ', $i + 1, '</th>';
				}
				echo '</tr>';
				foreach ($matrix[0] as $game => $teams) {
					echo '<tr><td><strong>', $game, '</strong></td>';
					for ($i = 0; $i < $runs; $i++) {
						$gameAmount = count($matrix[$i][$game]);
						if ($gameAmount === 0) {
							echo '<td class="pairingsCell"><br/><span/></span>';
						}
						for ($j = $gameAmount - 1; $j >= 0; $j--) {
							$team = $matrix[$i][$game][$j];
							echo '<td class="pairingsCell"><form method="post">',
									$team['name'],
									'<br/><span>Fails: <input type="text" name="fails-update-', $i, '-', $game, '" value="',
									$team['fails'],
									'"/></span><input type="submit" value="Update"></form></td>';
						}
					}
					echo '</tr>';
				}
				echo '</table><button onclick="requestPairingGenerationSimplified()" title="Generiert die Kombinationen von Spielen und Teams">
						Paarungen neu generieren</button>';
			} else { ?>
				<button onclick="requestPairingGenerationSimplified()" title="Generiert die Kombinationen von Spielen und Teams">
						Paarungen generieren</button>
			<?php } ?>
		</div>

		<script>
			addPlayer();
			addGame();
		</script>
	</body>
</html>
