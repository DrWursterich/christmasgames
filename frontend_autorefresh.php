<html>
	<head>
		<title>Weihnachtsfeierspiele Übersicht</title>
		<link rel="stylesheet" type="text/css" href="/styles/styles.css">
		<?php
			include 'DatabaseManager.php';
		?>
	</head>
	<body>
		<?php
			$teams = DatabaseManager::getTeams();
			$games = DatabaseManager::getGames();
			$matrix = DatabaseManager::getPairings($teams, $games);
			$runs = count($matrix);
		?>
		<div class="app"/>
			<div class="h"/>
				<img class="h-1" src="cynapsis_interactive.png" alt="Synapsis">
				<img class="h-2" src="logo_sitepark_type.gif" alt="Citepark">
			</div>
			<div class="datagrid">
				<table style="width: 100%;">
					<thead>
						<tr>
							<th></th>
							<?php
								foreach ($games as $game => $description) {
									echo '<th>', $game, '</th>';
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($matrix && is_array($matrix) && count($matrix) > 0) {
								$alt = true;
								foreach ($teams as $team => $object) {
									echo '<tr', $alt = !$alt ? ' class="alt"' : '', '><td><strong>', $team, '</strong></td>';
									foreach ($games as $game => $description) {
										$score = '';
										for ($i = count($matrix) - 1; $i >= 0; $i--) {
											if (count($matrix[$i][$game]) > 0 && $matrix[$i][$game][0]['name'] == $team) {
												$score = $matrix[$i][$game][0]['fails'];
											}
										}
										echo '<td class="pairingsCell"><span>',
												$score,
												'</span></td>';
									}
									echo '</tr>';
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<script>
			setTimeout(function(){
				window.location.reload(1);
			}, 20000);
		</script>
	</body>
</html>
